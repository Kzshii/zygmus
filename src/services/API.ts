import { Comment, Post } from '../models';

export const API = {
  async getPosts(): Promise<Post[]> {
    return fetch('https://jsonplaceholder.typicode.com/posts')
      .then((data) => data.json())
      .catch((err) => {
        console.error(err);
        return err;
      });
  },

  async getComments(): Promise<Comment[]> {
    return fetch('https://jsonplaceholder.typicode.com/comments')
      .then((data) => data.json())
      .catch((err) => {
        console.error(err);
        return err;
      });
  }
};
