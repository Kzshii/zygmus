import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { Store } from './store';
import AppInner from 'AppInner';

function App() {
  return (
    <Provider store={Store}>
      <AppInner />
    </Provider>
  );
}

export default App;
