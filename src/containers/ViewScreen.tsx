import React from 'react';
import styled from '@emotion/styled';
import { Post } from 'models';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Header } from 'components/Header';

const PostWrapper = styled(Link)<{ odd: boolean }>`
  padding: 6px;
  background: ${(p) => (p.odd ? '#e6e4e3' : 'white')};
`;

interface Props {
  posts: Post[];
}

const ViewScreen: React.FC<Props> = ({ posts }) => {
  return (
    <>
      <Header>Posts</Header>
      {posts.map((post, idx) => (
        <PostWrapper key={post.id} odd={!(idx % 2)} to={`/comment/${post.id}`}>
          {post.title}
        </PostWrapper>
      ))}
    </>
  );
};

const mapStateToProps = (state: any) => ({
  posts: state.postsState.posts
});

export default withRouter(connect(mapStateToProps)(ViewScreen));
