import React from 'react';
import { Comment } from '../models';
import styled from '@emotion/styled';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Input, Header, Button } from 'components';
import { addComment } from 'store/actions/commentsActions';

const Wrapper = styled.div`
  padding: 6px;
`;

const Title = styled.div`
  font-size: 20px;
  font-weight: 700;
`;

const Body = styled.div`
  font-size: 16px;
`;

const Email = styled.div`
  font-size: 18px;
  font-weight: 600;
`;

const ErrorMessage = styled.div`
  color: red;
  font-size: 18px;
  font-weight: 600;
`;

interface Props {
  comments: Comment[];
  addComment: (comment: Comment) => void;
  postId: string;
}

const CommentScreen: React.FC<Props> = ({ comments, addComment, postId }) => {
  const [fields, setFields] = React.useState({
    id: comments.length,
    postId: parseInt(postId),
    email: '',
    name: '',
    body: ''
  });
  const [error, setError] = React.useState<boolean>(false);

  const handleSendComment = () => {
    if (
      !!fields.body &&
      !!fields.name &&
      !!fields.email &&
      /^([a-z]){1,}([a-z0-9._-]){1,}([@]){1}([a-z]){2,}([.]){1}([a-z]){2,}([.]?){1}([a-z]?){2,}$/i.test(
        fields.email
      )
    ) {
      addComment(fields);
      setError(false);
      setFields((state) => ({
        ...state,
        id: state.id + 1,
        email: '',
        name: '',
        body: ''
      }));
    } else {
      setError(true);
    }
  };

  const handleChange = (field: keyof Record<string, string>, value: string) => {
    setFields((state) => ({ ...state, [field]: value }));
  };
  return (
    <>
      <Link to="/">Back to posts</Link>
      <Header>Comments</Header>
      {comments.map((comment, idx) => (
        <Wrapper data-testid={`comment-${idx}`} key={idx}>
          <Title>{comment.name}</Title>
          <Email>{comment.email}</Email>
          <Body>{comment.body}</Body>
        </Wrapper>
      ))}
      <Input
        placeholder="Insert your name"
        onChange={(value) => handleChange('name', value)}
        value={fields.name}
      />
      <Input
        type="email"
        placeholder="Insert your email"
        onChange={(value) => handleChange('email', value)}
        value={fields.email}
      />
      <Input
        placeholder="Your message here"
        onChange={(value) => handleChange('body', value)}
        value={fields.body}
      />
      {error && <ErrorMessage>Please fill the fields correctly</ErrorMessage>}
      <Button onClick={handleSendComment}>Send comment</Button>
    </>
  );
};

const mapStateToProps = (state: any, ownProps: any) => ({
  comments: state.commentsState.comments.filter(
    (c: Comment) => c.postId === parseInt(ownProps.match.params.postId)
  ),
  postId: ownProps.match.params.postId
});

const mapDispatchToProps = (dispatch: any) => ({
  addComment: (comment: Comment) => dispatch(addComment(comment))
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CommentScreen)
);
