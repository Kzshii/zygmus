import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { Input } from './Input';

test('should render input with default value', () => {
  const handleOnChange = jest.fn();
  render(<Input value="test" onChange={handleOnChange} />);

  expect(screen.getByDisplayValue('test')).toBeInTheDocument();
});

test('should call onchange with new value', () => {
  const handleOnChange = jest.fn();
  render(<Input value="test" onChange={handleOnChange} />);
  fireEvent.change(screen.getByDisplayValue('test'), {
    target: { value: 'test 2' }
  });

  expect(handleOnChange).toBeCalledWith('test 2');
});
