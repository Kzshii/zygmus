import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { Button } from './Button';

test('should call onClick', () => {
  const handleOnClick = jest.fn();
  render(<Button onClick={handleOnClick}>Test</Button>);
  fireEvent.click(screen.getByText('Test'));

  expect(handleOnClick).toBeCalled();
});
