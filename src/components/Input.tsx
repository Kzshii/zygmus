import styled from '@emotion/styled';
import React from 'react';

const InputElement = styled.input`
  padding: 6px 8px;
  border-style: transparent;
`;

interface Props {
  placeholder?: string;
  value: string | number;
  type?: string;
  onChange: (value: any) => void;
}

export const Input: React.FC<Props> = ({ onChange, ...rest }) => {
  return <InputElement onChange={(e) => onChange(e.target.value)} {...rest} />;
};
