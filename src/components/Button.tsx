import styled from '@emotion/styled';
import React from 'react';

const ButtonElement = styled.button`
  padding: 8px 12px;
  background: #1da1f2;
  border-radius: 6px;
  border-style: none;
  color: white;
`;

interface Props {
  onClick: () => void;
}

export const Button: React.FC<Props> = ({ children, ...rest }) => {
  return <ButtonElement {...rest}>{children}</ButtonElement>;
};
