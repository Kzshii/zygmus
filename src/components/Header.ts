import styled from '@emotion/styled';

export const Header = styled.div`
  font-size: 32px;
  font-weight: 700;
  text-align: center;
`;
