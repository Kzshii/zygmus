import React from 'react';
import { withRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.css';
import styled from '@emotion/styled';
import { Provider } from 'react-redux';
import { Store } from './store';
import CommentScreen from 'containers/CommentScreen';
import ViewScreen from 'containers/ViewScreen';
import { Comment, Post } from 'models';
import { addComments } from 'store/actions/commentsActions';
import { addPosts } from 'store/actions/postsActions';
import { API } from 'services/API';

const Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
  margin: 0;
  display: flex;
  flex-direction: column;
`;

interface Props {
  addComments: (comments: Comment[]) => void;
  addPosts: (posts: Post[]) => void;
  comments: Comment[];
}

const AppInner = ({ addPosts, addComments, comments }: Props) => {
  React.useEffect(() => {
    (async () => {
      if (comments.length === 0) {
        const posts = await API.getPosts();
        const comments = await API.getComments();
        addPosts(posts);
        addComments(comments);
      }
    })();
  }, [addPosts, addComments, comments.length]);
  return (
    <Provider store={Store}>
      <Wrapper>
        <Switch>
          <Route
            path="/comment/:postId"
            component={CommentScreen}
            exact={true}
          />
          <Route path="/" component={ViewScreen} />
        </Switch>
      </Wrapper>
    </Provider>
  );
};

const mapStateToProps = (state: any, ownProps: any) => ({
  comments: state.commentsState.comments,
  posts: state.postsState.posts
});

const mapDispatchToProps = (dispatch: any) => ({
  addPosts: (posts: Post[]) => dispatch(addPosts(posts)),
  addComments: (comments: Comment[]) => dispatch(addComments(comments))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppInner));
