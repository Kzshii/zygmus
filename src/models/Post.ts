export class Post {
  public userId: number = 0;
  public id: number = 0;
  public title: string = '';
  public body: string = '';

  constructor(props?: Partial<Post>) {
    props = props || {};
    Object.assign(this, props);
  }
}
