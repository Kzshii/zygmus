import { Post } from "models";
import { ADD_POSTS } from "store/actions/postsActions";

export interface PostsState {
  posts: Post[];
}

const initialState = {
  posts: []
};

export const postsReducer = (state = initialState, action: any): PostsState => {
  switch (action.type) {
    case ADD_POSTS:
      return {
        ...state.posts,
        posts: [
          ...action.payload
        ]
      };
    default:
      return state;
  }
};