import { commentsReducer } from './commentsReducer';
import { postsReducer } from './postsReducer';
import { combineReducers } from 'redux';
export const Reducers = combineReducers({
  commentsState: commentsReducer,
  postsState: postsReducer
});