import { ADD_COMMENT, ADD_COMMENTS } from "store/actions/commentsActions";
import { Comment } from "../../models";

export interface CommentsState {
  comments: Comment[];
}

const initialState = {
  comments: []
};

export const commentsReducer = (state = initialState, action: any): CommentsState => {
  switch (action.type) {
    case ADD_COMMENT:
      return {
        comments: [
          ...state.comments,
          action.payload
        ]
      };
    case ADD_COMMENTS:
      return {
        comments: action.payload
      };
    default:
      return state;
  }
};