import { Post } from "../../models";

export const ADD_POSTS = 'ADD_POSTS';

export const addPosts = (payload: Post[]) => ({
  type: ADD_POSTS,
  payload
});