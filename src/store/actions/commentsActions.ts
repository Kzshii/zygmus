import { Comment } from "../../models";

export const ADD_COMMENTS = 'ADD_COMMENTS';
export const ADD_COMMENT = 'ADD_COMMENT';

export const addComments = (payload: Comment[]) => ({
  type: ADD_COMMENTS,
  payload
});

export const addComment = (payload: Comment) => ({
  type: ADD_COMMENT,
  payload
})